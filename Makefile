
BUILD_DIR ?= $(abspath .build)
QMQTT_BUILD_DIR ?= $(abspath $(BUILD_DIR)/.qmqtt-build)
QMAKE ?= qmake
MKDIR = mkdir -p
MAKE_PARAMS = -j $$(nproc) --silent

first: build

help :
	@echo "The file contains the rules needed to build the application."
	@echo "Contains the following objectives:"
	@echo "  * build - build the application (by default)"
	@echo "  * desktop - build the option with SPI work imitation"
	@echo "  * qmqtt - build QMQTT"
	@echo "  * clean - removes build directories"
	@echo "  * help - show this text"
	@echo "Accepts the following environment values:"
	@echo "  * BUILD_DIR - application build directory"
	@echo "  * QMQTT_BUILD_DIR - The QMQTT build directory (must be an absolute path)"
	@echo "  * QMAKE - qmake command (if different from qmake)"

build : $(BUILD_DIR)/Makefile
	$(MAKE) $(MAKE_PARAMS) -C $(BUILD_DIR)

desktop : $(BUILD_DIR)/Makefile.desktop
	$(MAKE) $(MAKE_PARAMS) -C $(BUILD_DIR) -f Makefile.desktop

qmqtt : $(QMQTT_BUILD_DIR)/Makefile
	$(MAKE) $(MAKE_PARAMS) -C $(QMQTT_BUILD_DIR)

$(BUILD_DIR)/Makefile : $(BUILD_DIR) qmqtt
ifneq ($(wildcard $(BUILD_DIR)/Makefile.desktop),)
	$(MAKE) -C $(BUILD_DIR) -f Makefile.desktop distclean
endif
	cd $(BUILD_DIR) \
	&& $(QMAKE) -o `pwd`/Makefile ../src

$(BUILD_DIR)/Makefile.desktop : $(BUILD_DIR) qmqtt
ifneq ($(wildcard $(BUILD_DIR)/Makefile),)
	$(MAKE) -C $(BUILD_DIR) distclean
endif
	cd $(BUILD_DIR) \
	&& $(QMAKE) CONFIG+=DESKTOP -o `pwd`/Makefile.desktop ../src

$(QMQTT_BUILD_DIR)/Makefile : $(QMQTT_BUILD_DIR)
	cd $(QMQTT_BUILD_DIR) \
	&& $(QMAKE) CONFIG+=NO_UNIT_TESTS -o `pwd`/Makefile ../../qmqtt \

$(BUILD_DIR) :
	$(MKDIR) $(BUILD_DIR)/qmqtt
	ln -s $(QMQTT_BUILD_DIR)/include/qmqtt $(BUILD_DIR)/qmqtt/include
	ln -s $(QMQTT_BUILD_DIR)/lib $(BUILD_DIR)/qmqtt/
	ln -s include/qmqttDepends $(BUILD_DIR)/qmqtt/
	ln -s $(QMQTT_BUILD_DIR)/lib/libqmqtt.so.1 $(BUILD_DIR)
$(QMQTT_BUILD_DIR) :
	$(MKDIR) $(QMQTT_BUILD_DIR)

clean :
	rm -rf $(QMQTT_BUILD_DIR) $(BUILD_DIR)

PHONY : first help build desktop qmqtt clean
