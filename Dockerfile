FROM ubuntu:latest

RUN apt-get update && apt-get install -qqy make perl
ADD docker-files/armv7-eabihf--glibc--stable-2017.05-toolchains-1-1.tar.bz2 /opt/
ADD docker-files/qt5pi.tar.gz /usr/local/

ENV PATH=/opt/armv7-eabihf--glibc--stable/bin:/usr/local/qt5pi/bin:$PATH

