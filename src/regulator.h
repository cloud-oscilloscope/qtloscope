#ifndef REGULATOR_H
#define REGULATOR_H

#include <QObject>

class Regulator : public QObject
{
    Q_OBJECT
    enum State { Move, Zoom };
public:
    explicit Regulator(QObject *parent = nullptr);

    void setMinValue(int value);
    int minValue() const;

    void setMaxValue(int value);
    int maxValue() const;

    void setMoveStep(int value);
    int moveStep() const;

    void setZoomStep(int value);
    int zoomStep() const;

public slots:
    void up();
    void down();
    void changeState();

signals:
    void rangeChanged(int min, int max);

private:
    int m_minValue;
    int m_maxValue;

    int m_moveStep;
    int m_zoomStep;
    State m_state;
};

#endif // REGULATOR_H
