import QtQuick 2.7
import QtCharts 2.1

ChartView {
    id: chartView
    animationOptions: ChartView.NoAnimation
    theme: ChartView.ChartThemeDark
    property bool openGL: true
    property bool openGLSupported: true

    legend.visible: false
    margins.left: 2
    margins.top: 2
    margins.bottom: 16
    margins.right: 2

    onOpenGLChanged: {
        if (openGLSupported) {
            series("signal 1").useOpenGL = openGL;
        }
    }

    Component.onCompleted: {
        if (!series("signal 1").useOpenGL) {
            openGLSupported = false
            openGL = false
        }
    }

    Connections {
        target: regulatorY
        onRangeChanged: {
            console.log("Change range (", min, ", ", max, ")")
            axisY1.min = min
            axisY1.max = max
        }
    }

    ValueAxis {
        id: axisY1
        min: 0
        max: 4096
    }

    ValueAxis {
        id: axisX
        min: 0
        max: 150
    }

    LineSeries {
        id: lineSeries1
        name: "signal 1"
        axisX: axisX
        axisY: axisY1
        useOpenGL: chartView.openGL
    }

    Timer {
        id: refreshTimer
        interval: 1 / 50 * 1000 // 60 Hz
        running: true
        repeat: true
        onTriggered: {
            spiData.updateData(chartView.series(0));
        }
    }

//    function createAxis(min, max) {
//        // The following creates a ValueAxis object that can be then set as a x or y axis for a series
//        return Qt.createQmlObject("import QtQuick 2.0; import QtCharts 2.0; ValueAxis { min: "
//                                  + min + "; max: " + max + " }", chartView);
//    }

    function setAnimations(enabled) {
        if (enabled)
            chartView.animationOptions = ChartView.SeriesAnimations;
        else
            chartView.animationOptions = ChartView.NoAnimation;
    }

    function changeRefreshRate(rate) {
        refreshTimer.interval = 1 / Number(rate) * 1000;
    }
}
