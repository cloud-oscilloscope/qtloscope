import QtQuick 2.7

Item {
    id: main
    width: 480
    height: 320

    ScopeView {
        id: scopeView
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        height: main.height
    }

    Text {
        id: statusLine
        color: "white"
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        padding: 8
        Connections {
            target: dataHandler
            onMessageChanged:  statusLine.text = msg
        }
    }

    Text {
        id: clock
        color: "white"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        padding: 8
    }

    Timer {
        interval: 1000
        running: true
        repeat: true

        onTriggered: {
            clock.text = new Date().toLocaleTimeString(Qt.locale(), "hh:mm:ss")
        }
    }
}
