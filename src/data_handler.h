#ifndef DATA_HANDLER_H
#define DATA_HANDLER_H

#include <QObject>
#include <QPointF>
#include <QVector>
#include <QReadWriteLock>
#include <qmqtt>

class DataHandler : public QObject
{
    Q_OBJECT
public:
    enum State {
        Stop,
        Record
    };

    DataHandler(quint16 deviceId, const QString &server, qint64 port, quint64 maxNumberOfValues,
                QObject *parent = nullptr);

    quint16 deviceId() const { return m_deviceId; }
    void setDeviceId(quint16 id) { m_deviceId = id; }

signals:
    void messageChanged(const QString &msg);

public slots:
    void onData(const QVector<QPointF> &data);
    void changeState();
    void flushDownData();

private slots:
    void connected();
    void disconnected();
    void error(const QMQTT::ClientError error);
    void published(const QMQTT::Message& message, quint16 msgid = 0);

private:
    QMQTT::Client *mqtt();

    quint16 m_deviceId;
    QString m_server;
    qint64 m_port;
    quint64 m_maxNumberOfValues;
    State m_state;
    QReadWriteLock m_lockAllData;
    QVector<QPointF> m_allData;
    QReadWriteLock m_lockMqtt;
    QMQTT::Client *m_mqtt;
    bool m_mqttConnecting;

};

#endif // DATA_HANDLER_H
