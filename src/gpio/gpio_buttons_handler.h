#ifndef GPIO_BUTTONS_HANDLER_H
#define GPIO_BUTTONS_HANDLER_H

#include <QThread>

#include "gpio_buttons.h"

class GpioButtonsHandler : public QThread
{
    Q_OBJECT
public:
    GpioButtonsHandler(QObject *parent = nullptr);

    void appendButton(GpioButton *button);
    void stop();

protected:
    void run() Q_DECL_OVERRIDE;

private:
    volatile bool m_stopped;
    QVector<GpioButton *> m_buttons;
};

#endif // GPIO_BUTTONS_HANDLER_H
