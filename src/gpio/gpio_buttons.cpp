#include "gpio_buttons.h"

#include <QDebug>

GpioPushButton::GpioPushButton(int id, quint32 pin, QObject *parent)
    : GpioButton(parent)
    , m_value(true)
    , m_state(CLEAR)
{
    m_id = id;
    if (pin > 0) {
        m_pins.append(pin);
    }
    qDebug() << qPrintable(QString("GpioPushButton id=%1 pin").arg(m_id)) << m_pins;
}

void GpioPushButton::setPinValue(quint32 pin, bool value)
{
    if (m_pins.first() != pin) {
        return;
    }

    if (m_value != value) {
        m_value = value;
        if (value && m_state == PRESS) {
            m_state = CLEAR;
            qDebug() << "Button released. id:" << m_id;
            emit released();
            return;
        }
        if (!value && m_state == CLEAR) {
            m_state = PRESS;
            qDebug() << "Button pressed. id:" << m_id;
            emit pressed();
            return;
        }
    }
}

GpioSelectorButton::GpioSelectorButton(int id, quint32 leftPin, quint32 rightPin, QObject *parent)
    : GpioButton(parent)
    , m_leftPin(leftPin)
    , m_leftValue(true)
    , m_rightPin(rightPin)
    , m_rightValue(true)
    , m_state(CLEAR)
{
    m_id = id;
    if (m_leftPin > 0) {
        m_pins.append(m_leftPin);
    }
    if (m_rightPin > 0) {
        m_pins.append(m_rightPin);
    }
    qDebug() << qPrintable(QString("GpioSelectorButton id=%1 pins").arg(m_id)) << m_pins;
}

void GpioSelectorButton::setPinValue(quint32 pin, bool value)
{
    if (m_leftPin == pin && m_leftValue != value) {
        m_leftValue = value;
        if (m_state == CLEAR && !value) {
            m_state = LEFT;
            return;

        } else if (m_state == RIGHT && !value && !m_rightValue) {
            m_state = CLEAR;
            qDebug() << "Turn right. id:" << m_id;
            emit turnRight();
            return;
        }
        if (m_leftValue && m_rightValue) {
            m_state = CLEAR;
        }
    } if (m_rightPin == pin && m_rightValue != value) {
        m_rightValue = value;
        if (m_state == CLEAR && !value && m_leftValue) {
            m_state = RIGHT;

        } else if (m_state == LEFT && !value && !m_leftValue) {
            m_state = CLEAR;
            qDebug() << "Turn left. id:" << m_id;
            emit turnLeft();
        }
        if (m_leftValue && m_rightValue) {
            m_state = CLEAR;
        }
    }
}
