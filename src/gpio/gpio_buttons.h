#ifndef GPIO_BUTTON_H
#define GPIO_BUTTON_H

#include <QMap>
#include <QObject>
#include <QVector>

class GpioButton: public QObject
{
public:
    explicit GpioButton(QObject *parent = NULL)
        : QObject(parent)
        , m_id(-1)
    {
    }

    virtual ~GpioButton()
    {
    }

    QVector<quint32> pins() const
    {
        return  m_pins;
    }

    virtual void setPinValue(quint32 pin, bool value) = 0;

protected:
    int m_id;
    QVector<quint32> m_pins;
};


class GpioPushButton: public GpioButton
{
    Q_OBJECT

    enum State{
        CLEAR,
        PRESS,
    };

public:
    explicit GpioPushButton(int id = -1, quint32 pin = 0, QObject *parent = NULL);

    void setPinValue(quint32 pin, bool value);

signals:
    void pressed();
    void released();

private:
    bool m_value;
    State  m_state;
};

class GpioSelectorButton: public GpioButton
{
    Q_OBJECT

    enum State{
        CLEAR,
        LEFT,
        RIGHT
    };
public:
    explicit GpioSelectorButton(int id = -1, quint32 leftPin = 0, quint32 rightPin = 0,
                                QObject *parent = NULL);

    void setPinValue(quint32 pin, bool value);

signals:
    void turnLeft();
    void turnRight();

private:
    quint32 m_leftPin;
    bool m_leftValue;
    quint32 m_rightPin;
    bool m_rightValue;
    State m_state;
};

#endif // GPIO_BUTTON_H
