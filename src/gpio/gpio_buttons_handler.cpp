#include "gpio_buttons_handler.h"

#include "qtgpio.h"

#include <QDebug>
#include <QVector>

GpioButtonsHandler::GpioButtonsHandler(QObject *parent)
    : QThread(parent)
    , m_stopped(true)
{

}

void GpioButtonsHandler::appendButton(GpioButton *button)
{
    m_buttons.append(button);
}

void GpioButtonsHandler::stop()
{
    m_stopped = true;
}

void GpioButtonsHandler::run()
{
    m_stopped = false;
    IoController gpioController;

    foreach (const GpioButton *button, m_buttons) {
        foreach (const quint32 pin, button->pins()) {
            gpioController.exportGPIO(pin);
            gpioController.setDirection(pin, true);
        }
    }
    while(!m_stopped) {
        usleep(1000);
        for (int i = 0;  i < m_buttons.size(); ++i) {
            const QVector<quint32> &pins = m_buttons.at(i)->pins();
             for (int j = 0; j < pins.size(); ++j) {
                bool value = false;
                if (gpioController.value(pins.at(j), value)) {
                    m_buttons[i]->setPinValue(pins.at(j), value);
                }
            }
        }
    }
}
