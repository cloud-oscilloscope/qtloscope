QT += charts qml quick
CONFIG += qtquickcompiler debug

HEADERS += \
    gpio/gpio_buttons.h \
    gpio/gpio_buttons_handler.h \
    gpio/qtgpio.h \
    spi/mcp3008spi.h \
    spi/spi_handler.h\
    data_handler.h \
    regulator.h

SOURCES += \
    gpio/gpio_buttons.cpp \
    gpio/gpio_buttons_handler.cpp \
    gpio/qtgpio.cpp \
    spi/mcp3008spi.cpp \
    spi/spi_handler.cpp \
    data_handler.cpp \
    main.cpp \
    regulator.cpp

OBJECTS_DIR = .obj
MOC_DIR = .moc
RCC_DIR = .rcc

RESOURCES += \
    resources.qrc

DISTFILES += \
    qml/qmloscilloscope/*

unix: !mac: LIBS += -Wl,-rpath=./

# qmqtt
!exists($$OUT_PWD/qmqtt) : error(Reqared QmQtt. Fix me later)

INCLUDEPATH += $$OUT_PWD/qmqtt/include
LIBS += -L$$OUT_PWD/qmqtt/lib -lqmqtt

# desktop
DESKTOP {
    SOURCES -= spi/spi_handler.cpp
    SOURCES += spi/spi_handler-immitator.cpp
}
