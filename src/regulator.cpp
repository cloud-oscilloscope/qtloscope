#include "regulator.h"

Regulator::Regulator(QObject *parent)
    : QObject(parent)
    , m_minValue(0)
    , m_maxValue(1)
    , m_moveStep(1)
    , m_zoomStep(1)
    , m_state(Move)
{
}

void Regulator::setMinValue(int value)
{
    if (m_minValue == value) {
        return;
    }
    m_minValue = value;
    emit rangeChanged(m_minValue, m_maxValue);
}

int Regulator::minValue() const
{
    return m_minValue;
}

void Regulator::setMaxValue(int value)
{
    if (m_maxValue == value) {
        return;
    }
    m_maxValue = value;
    emit rangeChanged(m_minValue, m_maxValue);
}

void Regulator::setMoveStep(int value)
{
    m_moveStep = value;
}

int Regulator::moveStep() const
{
    return m_moveStep;
}

void Regulator::setZoomStep(int value)
{
    m_zoomStep = value;
}

int Regulator::zoomStep() const
{
    return m_zoomStep;
}

void Regulator::up()
{
    if (m_state == Move) {
        m_minValue += m_moveStep;
        m_maxValue += m_moveStep;
    } else {
        if ((m_maxValue - m_minValue) > m_zoomStep) {
            m_minValue += m_zoomStep;
            m_maxValue -= m_zoomStep;
        }
    }
    emit rangeChanged(m_minValue, m_maxValue);
}

void Regulator::down()
{
    if (m_state == Move) {
        m_minValue -= m_moveStep;
        m_maxValue -= m_moveStep;
    } else {
        m_minValue -= m_zoomStep;
        m_maxValue += m_zoomStep;
    }
    emit rangeChanged(m_minValue, m_maxValue);
}

void Regulator::changeState()
{
    m_state = m_state == Move ? Zoom : Move;
}
