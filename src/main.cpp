#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>

#include <QApplication>
#include <QtCore/QDir>
#include <QtCore/QSettings>

#include "gpio/gpio_buttons.h"
#include "gpio/gpio_buttons_handler.h"
#include "spi/spi_handler.h"
#include "data_handler.h"
#include "regulator.h"

int main(int argc, char *argv[])
{
    // Qt Charts uses Qt Graphics View Framework for drawing, therefore QApplication must be used.
    QApplication app(argc, argv);

    QSettings config("oscilloscope.ini", QSettings::IniFormat);

    config.beginGroup("LeftButton");
    GpioPushButton pushButton(Qt::Key_unknown + 1,
                              config.value("pushPin", 19).toInt());
    GpioSelectorButton selectorButton(Qt::Key_unknown + 2,
                                      config.value("leftPin", 7).toInt(),
                                      config.value("rightPin", 3).toInt());
    config.endGroup();

    config.beginGroup("CenterButton");
    GpioPushButton dataButton(Qt::Key_unknown + 3, config.value("pushPin", 12).toInt());
    config.endGroup();

    Regulator regulator;
    config.beginGroup("Regulator");
    regulator.setMinValue(config.value("minValue", 0).toInt());
    regulator.setMaxValue(config.value("maxValue", 4096).toInt());
    regulator.setMoveStep(config.value("moveStep", 32).toInt());
    regulator.setZoomStep(config.value("zoomValue", 128).toInt());
    config.endGroup();

    QObject::connect(&selectorButton, SIGNAL(turnLeft()), &regulator, SLOT(down()));
    QObject::connect(&selectorButton, SIGNAL(turnRight()), &regulator, SLOT(up()));
    QObject::connect(&pushButton, SIGNAL(released()), &regulator, SLOT(changeState()));

    GpioButtonsHandler gpioHandler;
    gpioHandler.appendButton(&pushButton);
    gpioHandler.appendButton(&selectorButton);
    gpioHandler.appendButton(&dataButton);
    gpioHandler.start();

    SpiHandler spiHandler;
    spiHandler.init();
    spiHandler.start();

    QQuickView viewer;

    QString extraImportPath(QStringLiteral("%1/../../../%2"));
    viewer.engine()->addImportPath(extraImportPath.arg(QGuiApplication::applicationDirPath(),
                                      QString::fromLatin1("qml")));
    QObject::connect(viewer.engine(), &QQmlEngine::quit, &viewer, &QWindow::close);

    config.beginGroup("MQTT");
    int deviceId = config.value("deviceId").toInt();
    if (deviceId == 0) {
        srand(QDateTime::currentSecsSinceEpoch());
        deviceId = static_cast<quint16>(rand());
        config.setValue("deviceId", deviceId);
    }
    DataHandler dataHandler(deviceId,
                            config.value("server", "iot.eclipse.org").toString(),
                            config.value("port", 1883).toInt(),
                            config.value("maxNumberOfValues", 10 * 1024 * 1024).toUInt());
    config.endGroup();

    QObject::connect(&spiHandler, SIGNAL(data(QVector<QPointF>)),
                     &dataHandler, SLOT(onData(QVector<QPointF>)));
    QObject::connect(&dataButton, SIGNAL(released()),
                     &dataHandler, SLOT(changeState()));

    viewer.rootContext()->setContextProperty("spiData", &spiHandler);
    viewer.rootContext()->setContextProperty("regulatorY", &regulator);
    viewer.rootContext()->setContextProperty("dataHandler", &dataHandler);

    viewer.setTitle(QStringLiteral("QML Oscilloscope"));
    viewer.setSource(QUrl("qrc:/qml/qmloscilloscope/main.qml"));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.setColor(QColor("#404040"));

    viewer.show();

    app.exec();

    spiHandler.stop();
    spiHandler.wait();

    gpioHandler.stop();
    gpioHandler.wait();

    return 0;
}
