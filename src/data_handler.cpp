#include "data_handler.h"

#include <QDebug>

DataHandler::DataHandler(quint16 deviceId, const QString &server, qint64 port, quint64 maxNumberOfValues,
                         QObject *parent)
    : QObject(parent)
    , m_deviceId(deviceId)
    , m_server(server)
    , m_port(port)
    , m_maxNumberOfValues(maxNumberOfValues == 0 ? 10 * 1024 * 1024 : maxNumberOfValues)
    , m_state(Stop)
    , m_mqtt(nullptr)
    , m_mqttConnecting(false)
{
    qDebug() << qPrintable(QString("[DataHandler]: deviceId=%1, server=%2, port=%3, maxNumberOfValues=%4")
                           .arg(m_deviceId).arg(m_server).arg(m_port).arg(m_maxNumberOfValues));
    m_allData.reserve(m_maxNumberOfValues);
}

void DataHandler::onData(const QVector<QPointF> &data)
{
    if (m_state == Stop) {
        return;
    }
    quint64 size = 0;

    {
        QReadLocker readLock(&m_lockAllData);
        size = m_allData.size();
    }
    if (size + data.size() >= m_maxNumberOfValues) {
        flushDownData();
    }

    {
        QReadLocker readLock(&m_lockAllData);
        size = m_allData.size();
    }
    if (size + data.size() >= m_maxNumberOfValues) {
        const QString msg("Buffer is full");
        qDebug() << qPrintable(msg);
        emit messageChanged(msg);
        return;
    }

    QWriteLocker lock(&m_lockAllData);
    m_allData.append(data);
}

void DataHandler::changeState()
{
    if (m_state == Record) {
        const QString msg("Uploading oscillogram");
        qDebug() << qPrintable(msg);
        emit messageChanged(msg);
        m_state = Stop;
        qDebug() << "Writed" << m_allData.size() << "values";

        flushDownData();

    } else if (m_state == Stop) {
        const QString msg("Writing oscillogram");
        qDebug() << qPrintable(msg);
        emit messageChanged(msg);
        m_state = Record;
        mqtt();
    }
}

void DataHandler::flushDownData()
{
    QMQTT::Client * mqtt = this->mqtt();
    if (!mqtt) {
        qDebug() << "Error! MQTT client isn't connected";
        return ;
    }

    QStringList dataList;
    {
        QWriteLocker lock(&m_lockAllData);
        foreach(QPointF p, m_allData) {
            dataList.append(QString::number(p.y()));
        }
        m_allData.resize(0);
    }

    QByteArray payload = "{\"id\":\"Osc-"
            + QByteArray::number(m_deviceId)
            + "\", \"data\":\"["
            + dataList.join(",").toLatin1()
            + "]\",\"meta\":\""
            + QDateTime::currentDateTime().toString("yy-MM-dd-hh-mm-ss").toLatin1()
            + "\"}";
    QMQTT::Message message = QMQTT::Message(m_deviceId, "cloud_oscilloscope/device/" + QString::number(m_deviceId), payload);
    mqtt->publish(message);
    const QString msg = QString("MQTT sended %1 values").arg(QString::number(dataList.size()));
    qDebug() << qPrintable(msg);
    emit messageChanged(msg);
}

QMQTT::Client *DataHandler::mqtt()
{
    {
        QReadLocker readLocker(&m_lockMqtt);
        if (m_mqtt && m_mqtt->isConnectedToHost()) {
            return m_mqtt;
        }
    }
    QWriteLocker lock(&m_lockMqtt);
    if (!m_mqtt) {
        qDebug() << "Creating MQTT client";
        m_mqtt = new QMQTT::Client(QHostAddress(m_server), m_port, this);
        if (!m_mqtt) {
            qDebug() << "Failed creating MQTT client";
            return nullptr;
        }
        connect(m_mqtt, SIGNAL(connected()), this, SLOT(connected()));
        connect(m_mqtt, SIGNAL(disconnected()), this, SLOT(disconnected()));
        connect(m_mqtt, SIGNAL(error(const QMQTT::ClientError)), this, SLOT(error(const QMQTT::ClientError)));
        connect(m_mqtt, SIGNAL(published(const QMQTT::Message, quint16)), this, SLOT(published(const QMQTT::Message, quint16)));
        m_mqtt->setHostName(m_server);
        m_mqtt->setAutoReconnect(true);
    }
    if (!m_mqtt->isConnectedToHost()) {
        if (!m_mqttConnecting && m_mqtt->connectionState() != QMQTT::STATE_CONNECTING) {
            const QString msg = QString("MQTT connecting %1:%2").arg(m_mqtt->hostName())
                    .arg(QString::number(m_mqtt->port()));
            qDebug() << qPrintable(msg);
            emit messageChanged(msg);
            //! TODO: этот костыль. надо будет пофиксить нормально.
            m_mqttConnecting = true;
            m_mqtt->connectToHost();
        }
    }
    return m_mqtt;
}

void DataHandler::connected()
{
    const QString msg("MQTT connected");
    qDebug() << qPrintable(msg);
    emit messageChanged(msg);
}

void DataHandler::disconnected()
{
    const QString msg("MQTT disconnected");
    qDebug() << qPrintable(msg);
    emit messageChanged(msg);
}

void DataHandler::error(const QMQTT::ClientError error)
{
    const QString msg("MQTT error " + error);
    qDebug() << qPrintable(msg);
    emit messageChanged(msg);
}

void DataHandler::published(const QMQTT::Message &message, quint16 msgid)
{
    Q_UNUSED(message)
    Q_UNUSED(msgid)

    const QString msg("MQTT published");
    qDebug() << qPrintable(msg);
    emit messageChanged(msg);
}
