#include "spi_handler.h"
#include "mcp3008spi.h"

#include <stdio.h>

#include <QMutexLocker>
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>

#include <QDebug>

namespace {

const int DATA_SIZE = 1024;

}

SpiHandler::SpiHandler(QObject *parent)
    :QThread(parent)
    , m_stopped(true)
    , m_x(0)
{
    // TEMP
    m_data.reserve(DATA_SIZE);
}

SpiHandler::~SpiHandler()
{
}

void SpiHandler::init()
{
    spi.reset(new mcp3008Spi("/dev/spidev0.0", SPI_MODE_0, 1000000, 8));
}

void SpiHandler::stop()
{
    m_stopped = true;
}

void SpiHandler::updateData(QAbstractSeries *series)
{
    if (series) {
        QMutexLocker locker(&m_mutex);
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        xySeries->replace(m_data);
        m_x = 0;
        emit data(m_data);
        m_data.clear();
        m_time.restart();
    }
}

void SpiHandler::run()
{
    m_stopped = false;

    int value = 0;
    int a2dChannel = 0; // Хз зачем эта хрень
    unsigned char data[3];
    m_x = 0;
    m_time.start();

    while (!m_stopped) {
        data[0] = 1;  //  first byte transmitted -> start bit
        data[1] = 0b10000000 |( ((a2dChannel & 7) << 4)); // second byte transmitted -> (SGL/DIF = 1, D2=D1=D0=0)
        data[2] = 0; // third byte transmitted....don't care

        spi->spiWriteRead(data, sizeof(data) );

        value = (static_cast<int>(data[0] & 0x1f) << 7) | (data[1] >> 1);
        addValue(value);
    }
}

void SpiHandler::addValue(int value)
{
    if (m_x < DATA_SIZE) {
        QMutexLocker locker(&m_mutex);
        m_data.append(QPoint(m_time.elapsed(), value));
        ++m_x;
    }
}
