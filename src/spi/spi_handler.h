#ifndef SPI_HANDLER_H
#define SPI_HANDLER_H

#include <QtCharts/QAbstractSeries>
#include <QMutex>
#include <QThread>
#include <QTime>
#include <QVector>

QT_CHARTS_USE_NAMESPACE

class mcp3008Spi;

Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)

class SpiHandler : public QThread
{
    Q_OBJECT
public:
    SpiHandler(QObject *parent = nullptr);
    ~SpiHandler();

    void init();
    void stop();

signals:
    void data(const QVector<QPointF> data);

public :
    Q_INVOKABLE void updateData(QAbstractSeries *series);

protected:
    void run();

private:
    void addValue(int value);

private:
    volatile bool m_stopped;
    QVector<QPointF> m_data;
    int m_x;
    QMutex m_mutex;
    QScopedPointer<mcp3008Spi> spi;
    QTime m_time;
};

#endif // SPI_HANDLER_H
