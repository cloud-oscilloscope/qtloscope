#include "spi_handler.h"

#include <stdio.h>

#include <QMutexLocker>
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>

#include <QDebug>

namespace {

const int DATA_SIZE = 1024;

}

class mcp3008Spi
{
};

SpiHandler::SpiHandler(QObject *parent)
    :QThread(parent)
    , m_stopped(true)
    , m_x(0)
{
    // TEMP
    m_data.reserve(DATA_SIZE);
}

SpiHandler::~SpiHandler()
{
}

void SpiHandler::init()
{
}

void SpiHandler::stop()
{
    m_stopped = true;
}

void SpiHandler::updateData(QAbstractSeries *series)
{
    if (series) {
        QMutexLocker locker(&m_mutex);
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        xySeries->replace(m_data);
        m_x = 0;
        emit data(m_data);
        m_data.clear();
        m_time.restart();
    }
}

void SpiHandler::run()
{
    m_stopped = false;
    m_x = 0;
    m_time.start();

    while (!m_stopped) {
        usleep(300);
        int value = static_cast<int>(sin(static_cast<double>(m_x)/5.0 + 1) * 2000);
        addValue(value);
    }
}

void SpiHandler::addValue(int value)
{
    if (m_x < DATA_SIZE) {
        QMutexLocker locker(&m_mutex);
        m_data.append(QPoint(m_time.elapsed(), value));
        ++m_x;
    }
}
